import React, { Component } from "react";
import "./App.css";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import "./styles/Main.css";
import BoardsPage from "./Components/BoardsPage";
import Navbar from "./Components/Navbar";
import EachBoardPage from "./Components/EachBoardPage";
import { Redirect } from "react-router-dom";

export class App extends Component {
  render() {
    return (
      <Router>
        <React.Fragment>
          <Navbar />
          <Switch>
            <Route exact path="/">
              <Redirect to="/boards" />
            </Route>
            <Route exact path="/boards" component={BoardsPage} />
            <Route exact path="/boards/:boardId" component={EachBoardPage} />
          </Switch>
        </React.Fragment>
      </Router>
    );
  }
}

export default App;
