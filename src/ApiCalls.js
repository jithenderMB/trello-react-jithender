import axios from "axios";

// const APIKey = 'APIKey';

// const APIToken = 'APIToken';

const fetchBoardsData = () => {
  return axios.get(
    "https://api.trello.com/1/members/me/boards?key=APIKey&token=APIToken"
  );
};

const addNewBoard = (boardName) => {
  if (boardName) {
    return axios.post(
      `https://api.trello.com/1/boards/?name=${boardName}&key=APIKey&token=APIToken`
    );
  }
};

const getListsOfBoard = (boardId) => {
  return axios.get(
    `https://api.trello.com/1/boards/${boardId}/lists?key=APIKey&token=APIToken`
  );
};

const createListInBoard = (boardId, listName) => {
  if (listName) {
    return axios.post(
      `https://api.trello.com/1/boards/${boardId}/lists?name=${listName}&key=APIKey&token=APIToken`
    );
  }
};

const deleteListInBoard = (listId) => {
  return axios.put(
    `https://api.trello.com/1/lists/${listId}/closed?value=true&key=APIKey&token=APIToken`
  );
};

const getAllCardsInBoard = (listId) => {
  return axios.get(
    `https://api.trello.com/1/lists/${listId}/cards?key=APIKey&token=APIToken`
  );
};

const createCardInList = (cardTitle, listId) => {
  if (cardTitle) {
    return axios.post(
      `https://api.trello.com/1/cards?name=${cardTitle}&idList=${listId}&key=APIKey&token=APIToken`
    );
  }
};

const deleteCardInList = (cardId) => {
  return axios.delete(
    `https://api.trello.com/1/cards/${cardId}?key=APIKey&token=APIToken`
  );
};

const getCheckListsInCard = (cardId) => {
  return axios.get(
    `https://api.trello.com/1/cards/${cardId}/checklists?key=APIKey&token=APIToken`
  );
};

const createCheckListInCard = (checkListTitle, cardId) => {
  if (checkListTitle) {
    return axios.post(
      `https://api.trello.com/1/checklists?name=${checkListTitle}&idCard=${cardId}&key=APIKey&token=APIToken`
    );
  }
};

const deleteCheckListInCard = (checkListId) => {
  return axios.delete(
    `https://api.trello.com/1/checklists/${checkListId}?key=APIKey&token=APIToken`
  );
};

const getCheckItemsInCheckList = (checkListId) => {
  return axios.get(
    `https://api.trello.com/1/checklists/${checkListId}/checkItems?key=APIKey&token=APIToken`
  );
};

const updateCheckItemInCheckList = (cardId, checkItemId, checkedStatus) => {
  return axios.put(
    `https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?state=${checkedStatus}&key=APIKey&token=APIToken`
  );
};

const createCheckItemInChecklist = (checkListId,checkItemTitle) => {
  if (checkItemTitle) {
    return axios.post(
      `https://api.trello.com/1/checklists/${checkListId}/checkItems?name=${checkItemTitle}&key=APIKey&token=APIToken`
    );
  }
};

const deleteCheckItemInCheckList = (checkListId,checkItemId) => {
  return axios.delete(`https://api.trello.com/1/checklists/${checkListId}/checkItems/${checkItemId}?key=APIKey&token=APIToken`)
}

export {
  fetchBoardsData,
  addNewBoard,
  getListsOfBoard,
  createListInBoard,
  deleteListInBoard,
  getAllCardsInBoard,
  createCardInList,
  deleteCardInList,
  getCheckListsInCard,
  createCheckListInCard,
  deleteCheckListInCard,
  getCheckItemsInCheckList,
  updateCheckItemInCheckList,
  createCheckItemInChecklist,
  deleteCheckItemInCheckList
};
