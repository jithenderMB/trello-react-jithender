import React, { Component } from "react";
import PlusIcon from "../images/plus-icon.svg";
import DeleteIcon from "../images/cross-logo.svg";
import {
  deleteListInBoard,
  getAllCardsInBoard,
  createCardInList,
  deleteCardInList,
} from "../ApiCalls";
import EachCard from "./EachCard";

class EachList extends Component {
  state = {
    cards: [],
    displayCardInput: false,
    userInput: "",
  };

  componentDidMount = () => {
    getAllCardsInBoard(this.props.eachList.id)
      .then((res) => res.data)
      .then((data) => {
        this.setState({
          cards: data,
        });
      });
  };

  handleNewCardInput = () => {
    this.setState({
      displayCardInput: true,
    });
  };
  handleAddCardOption = () => {
    this.setState({
      displayCardInput: false,
      userInput: "",
    });
  };

  handleDelete = () => {
    deleteListInBoard(this.props.eachList.id)
      .then((res) => res.data.id)
      .then((id) => {
        this.props.deleteList(id);
      });
  };

  handleNewCardTitle = (event) => {
    this.setState({
      userInput: event.target.value,
    });
  };

  addNewCardInList = (event) => {
    event.preventDefault();
    createCardInList(this.state.userInput, this.props.eachList.id)
      .then((res) => res.data)
      .then((data) => {
        this.setState({
          cards: [...this.state.cards, data],
          displayCardInput: false,
          userInput: "",
        });
      });
  };

  handleDeleteCard = (cardId) => {
    deleteCardInList(cardId).then((res) => {
      let newArray = this.state.cards.filter((card) => card.id !== cardId);
      this.setState({
        cards: newArray,
      });
    });
  };
  render() {
    return (
      <div className="m-2">
        <div className="board-list-container">
          <div className="d-flex justify-content-between">
            <h6>{this.props.eachList.name}</h6>
            <img
              onClick={this.handleDelete}
              className="board-list-close-icon align-self-start pointerCursor"
              src={DeleteIcon}
              alt="cross-icon.svg"
            />
          </div>
          {this.state.cards.map((card) => (
            <EachCard
              key={card.id}
              cardDetails={card}
              listDetails={this.props.eachList}
              deleteCard={this.handleDeleteCard}
            />
          ))}

          <div
            onClick={this.handleNewCardInput}
            className={
              this.state.displayCardInput
                ? "d-none"
                : "board-list-add-card-container d-flex align-items-center"
            }
          >
            <img
              className="board-list-add-card-icon"
              src={PlusIcon}
              alt="plus-icon.svg"
            />
            Add a card
          </div>
          <form
            onSubmit={this.addNewCardInList}
            className={this.state.displayCardInput ? "d-block" : "d-none"}
          >
            <textarea
              value={this.state.userInput}
              className="board-list-add-card-input col-12"
              placeholder="Enter a title for this card..."
              onChange={this.handleNewCardTitle}
            />
            <button className="btn-sm btn btn-primary">Add card</button>
            <button
              onClick={this.handleAddCardOption}
              className="board-list-add-card-input-close-button btn-sm btn btn-secondary"
            >
              Close
            </button>
          </form>
        </div>
      </div>
    );
  }
}

export default EachList;
