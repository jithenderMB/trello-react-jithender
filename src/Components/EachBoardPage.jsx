import React, { Component } from "react";
import EachList from "./EachList";
import PlusIcon from "../images/plus-icon.svg";
import { getListsOfBoard, createListInBoard } from "../ApiCalls";
import { Spinner } from "react-bootstrap";

class EachBoardPage extends Component {
  state = {
    lists: [],
    addListInput: false,
    userInput: "",
    spinner: true,
  };

  handleNewListInput = () => {
    this.setState({
      addListInput: true,
    });
  };
  handleNewInputClose = () => {
    this.setState({
      addListInput: false,
      userInput: "",
    });
  };

  handleListDelete = (Listid) => {
    let newArray = this.state.lists.filter((list) => list.id !== Listid);
    this.setState({
      lists: newArray,
    });
  };
  addNewList = (event) => {
    event.preventDefault();

    createListInBoard(this.props.match.params.boardId, this.state.userInput)
      .then((res) => res.data)
      .then((data) => {
        this.setState({
          lists: [...this.state.lists, data],
          addListInput: false,
          userInput: "",
        });
      });
  };
  updateNewListTitle = (event) => {
    this.setState({
      userInput: event.target.value,
    });
  };

  componentDidMount = () => {
    getListsOfBoard(this.props.match.params.boardId)
      .then((res) => res.data)
      .then((data) => {
        this.setState({
          lists: data,
          spinner: false,
        });
      });
  };
  render() {
    return (
      <div className="board-main-container d-flex">
        <div
          className={
            this.state.spinner ? "trello-spinner-board-page" : "d-none"
          }
        >
          <Spinner animation="border" />
        </div>
        {this.state.lists.map((list) => (
          <EachList
            key={list.id}
            eachList={list}
            deleteList={this.handleListDelete}
          />
        ))}
        <div className={this.state.spinner ? "d-none" : "m-2"}>
          <div
            className={
              this.state.addListInput ? "d-none" : "board-add-list-container"
            }
            onClick={this.handleNewListInput}
          >
            <img
              className="board-add-list-icon"
              src={PlusIcon}
              alt="plus-icon.svg"
            />
            Add a card
          </div>
          <form
            className={
              this.state.addListInput
                ? "board-add-list-input-container"
                : "d-none"
            }
            onSubmit={this.addNewList}
          >
            <input
              onChange={this.updateNewListTitle}
              value={this.state.userInput}
              className="col-12 mb-2 form-control"
              type="text"
              placeholder="Enter list title..."
            />
            <div>
              <button className="btn btn-primary btn-sm">Add list</button>
              <button
                className="board-add-list-close-button btn btn-secondary btn-sm"
                onClick={this.handleNewInputClose}
              >
                Close
              </button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default EachBoardPage;
