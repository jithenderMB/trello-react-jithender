import React, { Component } from "react";
import { Modal } from "react-bootstrap";
import DeleteIcon from "../images/cross-logo.svg";

import EachCheckList from "./EachCheckList";
import {
  getCheckListsInCard,
  createCheckListInCard,
  deleteCheckListInCard,
} from "../ApiCalls";

class EachCard extends Component {
  state = {
    checkLists: [],
    model: false,
    addCheckListInput: false,
    addCheckListButton: true,
    newCheckListTitle: "",
  };

  componentDidMount = () => {
    getCheckListsInCard(this.props.cardDetails.id)
      .then((res) => res.data)
      .then((data) => {
        this.setState({
          checkLists: data,
        });
      });
  };
  handleModelOpen = () => {
    this.setState({
      model: true,
    });
  };
  handleModelClose = () => {
    this.setState({
      model: false,
    });
  };

  handleAddCheckListInputOpen = () => {
    this.setState({
      addCheckListInput: true,
      addCheckListButton: false,
    });
  };
  handleAddCheckListInputClose = () => {
    this.setState({
      newCheckListTitle: "",
      addCheckListInput: false,
      addCheckListButton: true,
    });
  };
  addNewCheckList = (event) => {
    event.preventDefault();
    createCheckListInCard(
      this.state.newCheckListTitle,
      this.props.cardDetails.id
    )
      .then((res) => res.data)
      .then((data) => {
        this.setState({
          checkLists: [...this.state.checkLists, data],
          addCheckListInput: false,
          addCheckListButton: true,
          newCheckListTitle: "",
        });
      });
  };
  handleNewCheckListTitle = (event) => {
    this.setState({
      newCheckListTitle: event.target.value,
    });
  };
  handleDeleteCheckList = (checkListId) => {
    deleteCheckListInCard(checkListId)
      .then((res) => res)
      .then(() => {
        let newArray = this.state.checkLists.filter(
          (eachCheckList) => eachCheckList.id !== checkListId
        );
        this.setState({
          checkLists: newArray,
        });
      });
  };
  render() {
    return (
      <>
        <div className="trello-each-card-container d-flex justify-content-between">
          <div onClick={this.handleModelOpen} className="pointerCursor col-11">
            <p className="m-0">{this.props.cardDetails.name}</p>
          </div>
          <img
            className="pointerCursor"
            onClick={() => this.props.deleteCard(this.props.cardDetails.id)}
            src={DeleteIcon}
            alt="cross-icon.svg"
          />
        </div>

        <Modal show={this.state.model} onHide={this.handleModelClose}>
          <div className="trello-checklist-close-container d-flex justify-content-end">
            <img
              onClick={this.handleModelClose}
              className="trello-checklist-close-icon pointerCursor"
              src={DeleteIcon}
              alt="cross-icon.svg"
            />
          </div>
          <div className="trello-checklist-main-container">
            <div className="d-flex justify-content-between">
              <div>
                <h5>{this.props.cardDetails.name}</h5>
                <p className="trello-checklist-list-title">
                  in the {this.props.listDetails.name}
                </p>
              </div>
              <div>
                <button
                  onClick={this.handleAddCheckListInputOpen}
                  className={
                    this.state.addCheckListButton
                      ? "btn btn-primary btn-sm"
                      : "d-none"
                  }
                >
                  Add checkList
                </button>
                <button
                  onClick={this.handleAddCheckListInputClose}
                  className={
                    this.state.addCheckListButton
                      ? "d-none"
                      : "btn btn-secondary btn-sm"
                  }
                >
                  Close
                </button>
              </div>
            </div>
            <form
              onSubmit={this.addNewCheckList}
              className={
                this.state.addCheckListInput ? "d-flex mb-2" : "d-none"
              }
            >
              <input
                onChange={this.handleNewCheckListTitle}
                value={this.state.newCheckListTitle}
                type="text"
                className="form-control"
                placeholder="Enter a checklist title"
              />
              <button
                type="submit"
                className="trello-add-checklist-add-button btn btn-dark btn-sm"
              >
                Add
              </button>
            </form>
            <div>
              {this.state.checkLists.map((checkList) => (
                <EachCheckList
                  key={checkList.id}
                  checkListDetails={checkList}
                  cardDetails={this.props.cardDetails}
                  deleteCheckList={this.handleDeleteCheckList}
                />
              ))}
            </div>
          </div>
        </Modal>
      </>
    );
  }
}

export default EachCard;
