import React, { Component } from "react";
import { Link } from "react-router-dom";

export class Navbar extends Component {
  render() {
    return (
      <nav className="trello-navbar col-12 d-flex">
        <Link className="trello-navbar-home different" to="/boards">
          Home
        </Link>
        <Link className="trello-navbar-logo-container" to="/boards">
          <div className="trello-navbar-logo"></div>
        </Link>
      </nav>
    );
  }
}

export default Navbar;
