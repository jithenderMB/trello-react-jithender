import React, { Component } from "react";
import EachBoard from "./EachBoard";
import { fetchBoardsData, addNewBoard } from "../ApiCalls";
import { Modal, Spinner } from "react-bootstrap";
import userIcon from "../images/user-icon.svg";

class BoardsPage extends Component {
  state = {
    boards: [],
    modelPopUp: false,
    newBoardTitle: "",
    spinner: true
  };

  getBoardsDataUsingApi = () => {
    fetchBoardsData().then((res) =>
      this.setState({
        boards: res.data,
        spinner: false
      })
    );
  };
  componentDidMount = () => {
    this.getBoardsDataUsingApi();
  };

  handleNewBoardTitle = (event) => {
    this.setState({
      newBoardTitle: event.target.value,
    });
  };

  addNewBoard = () => {
    this.state.boards.length === 10
      ? alert("Boards limit exceeded")
      : addNewBoard(this.state.newBoardTitle)
          .then((res) => res.data)
          .then((data) => {
            this.setState({
              boards: [...this.state.boards, data],
              modelPopUp: false,
              newBoardTitle: "",
            });
          });
  };

  handleAddNewBoard = (event) => {
    event.preventDefault();
    this.addNewBoard();
  };

  handleOnEnter = (event) => {
    if (event.key === "Enter") {
      this.addNewBoard();
    }
  };

  handleAddBoardForm = () => {
    this.setState({
      modelPopUp: true,
    });
  };

  handleModelPopUp = () => {
    this.setState({
      modelPopUp: false,
      newBoardTitle: "",
    });
  };

  render() {
    return (
      <React.Fragment>
        <Modal show={this.state.modelPopUp} onHide={this.handleModelPopUp}>
          <form
            className="trello-create-board-container"
            onSubmit={this.handleAddNewBoard}
          >
            <h4>Create board</h4>
            <hr />
            <label htmlFor="boardTitleInputElement" className="mt-3">
              Board title
            </label>
            <input
              onKeyDown={this.handleOnEnter}
              type="text"
              onChange={this.handleNewBoardTitle}
              value={this.state.newBoardTitle}
              id="boardTitleInputElement"
              className="col-12 form-control mt-2"
              placeholder="Enter a board title"
            />
            <div className="trello-create-board-buttons-container d-flex justify-content-end">
              <button
                onClick={this.handleModelPopUp}
                className="trello-create-board-close-button btn btn-secondary btn-sm"
              >
                Close
              </button>
              <button type="submit" className="btn btn-primary btn-sm col-4">
                Add Board
              </button>
            </div>
          </form>
        </Modal>

        <div className="trello-boards-container">
          <div className="trello-boards-heading-container d-flex">
            <img className="trello-boards-heading-icon" src={userIcon} alt="user-icon.svg" />
            <h6 className="trello-boards-heading">Your Boards</h6>
          </div>
          <div className=" d-flex flex-wrap ">
            <div className={this.state.spinner?"trello-spinner":'d-none'}>
              <Spinner animation="border" />
            </div>

            {this.state.boards.map((board) => (
              <EachBoard key={board.id} eachBoard={board} />
            ))}
            <div
              onClick={this.handleAddBoardForm}
              className={!this.state.spinner?"trello-boards-create-board d-flex flex-column justify-content-center align-items-center":'d-none'}
            >
              <p>Create new board</p>
              <p className="trello-boards-count">
                Boards left: {10 - this.state.boards.length}
              </p>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default BoardsPage;
