import React, { Component } from "react";
import DeleteIcon from "../images/cross-logo.svg";
import checklistIcon from "../images/checklist-icon.svg";

import CheckItem from "./CheckItem";
import {
  getCheckItemsInCheckList,
  createCheckItemInChecklist,
  deleteCheckItemInCheckList,
} from "../ApiCalls";

class EachCheckList extends Component {
  state = {
    checkItems: [],
    checkItemsInputDisplay: false,
    checkItemInput: "",
  };
  componentDidMount = () => {
    getCheckItemsInCheckList(this.props.checkListDetails.id)
      .then((res) => res.data)
      .then((data) => {
        this.setState({
          checkItems: data,
        });
      });
  };
  handleCheckItemOpen = () => {
    this.setState({
      checkItemsInputDisplay: true,
    });
  };
  handleCheckItemClose = () => {
    this.setState({
      checkItemsInputDisplay: false,
    });
  };
  addNewCheckItem = (event) => {
    event.preventDefault();
    createCheckItemInChecklist(
      this.props.checkListDetails.id,
      this.state.checkItemInput
    )
      .then((res) => res.data)
      .then((data) => {
        this.setState({
          checkItems: [...this.state.checkItems, data],
          checkItemsInputDisplay: false,
          checkItemInput: "",
        });
      });
  };

  handleCheckItemDelete = (checkItemId) => {
    deleteCheckItemInCheckList(this.props.checkListDetails.id, checkItemId)
      .then((res) => res)
      .then((data) => {
        let newArray = this.state.checkItems.filter(
          (checkItem) => checkItem.id !== checkItemId
        );
        this.setState({
          checkItems: newArray,
        });
      });
  };

  handleCheckItemStatus = (checkItemDetails) => {
    let newArray = this.state.checkItems.map((checkItem) =>
      checkItem.id === checkItemDetails.id ? checkItemDetails : checkItem
    );
    this.setState({
      checkItems: newArray,
    });
  };
  handleCheckItemInput = (event) => {
    this.setState({
      checkItemInput: event.target.value,
    });
  };
  render() {
    return (
      <div className="trello-each-checklist-container mb-3">
        <div className="d-flex justify-content-between align-items-center">
          <div className="d-flex">
            <img
              className="trello-checklist-icon"
              src={checklistIcon}
              alt="checklist-icon.svg"
            />
            <h6 className="mt-2">{this.props.checkListDetails.name}</h6>
          </div>
          <img
            className="pointerCursor"
            onClick={() =>
              this.props.deleteCheckList(this.props.checkListDetails.id)
            }
            src={DeleteIcon}
            alt="close-icon.svg"
          />
        </div>
        <div>
          {this.state.checkItems.map((checkItem) => (
            <CheckItem
              key={checkItem.id}
              cardDetails={this.props.cardDetails}
              checkItemsDetails={checkItem}
              checkItemStatus={this.handleCheckItemStatus}
              deleteCheckItem={this.handleCheckItemDelete}
            />
          ))}
        </div>
        <button
          onClick={this.handleCheckItemOpen}
          className={
            this.state.checkItemsInputDisplay
              ? "d-none"
              : "mt-2 text-light btn btn-info btn-sm"
          }
        >
          Add an item
        </button>

        <form
          onSubmit={this.addNewCheckItem}
          className={this.state.checkItemsInputDisplay ? "mt-2" : "d-none"}
        >
          <textarea
            onChange={this.handleCheckItemInput}
            value={this.state.checkItemInput}
            className="trello-checklist-add-checkitem-input form-control"
            type="text"
            placeholder="Add an item"
          />
          <div>
            <button className="btn btn-primary btn-sm">Add</button>

            <button
              onClick={this.handleCheckItemClose}
              className="trello-checklist-add-checkitem-close-button btn btn-secondary btn-sm"
            >
              Cancel
            </button>
          </div>
        </form>
      </div>
    );
  }
}

export default EachCheckList;
